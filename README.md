Ein Framework für Joomla-Anwendungen von Gratia Mira.

Funktionen, die bereitgestellt werden

# Allgemeine Hilfen
Werden in der Klasse GMF_Allgemein bereitgestellt.
## istAdmin
```
/**
 * Prüft, ob ein Admin angemeldet ist.
 *
 * @return bool
 * @throws Exception
 * @since 0.1.0
 */
public static function istAdmin(): bool
```
## istBot
 ```
/**
 * Prüft, ob ein Bot oder ein Benutzer auf die Seite zugreift.
 *
 * @param   bool  $NurIntern Alle externen Benutzer und Direktzugriffe als Bot einstufen.
 *
 * @return bool
 * @link https://stackoverflow.com/questions/677419/how-to-detect-search-engine-bots-with-php
 * @since 0.0.9
 */
public static function istBot(): bool
```
## umbenennenzuAliasname
```
/**
 * Verändert ein String oder den Text einen einfachen Array in Aliasgerechte Namen. Im Moment werden alle Sonderzeichen mit Ausnahme von ä,ö,ü einfach weggelöscht.
 *
 * @param   string|array  $Wert  Die Daten welche zu Aliasnamen verändert werden sollen
 * @param   string        $Sprache
 *
 * @return string|array
 * @since   0.0.1
 * @version 0.0.7
 *
 * @todo Standard Joomla-Funktion für Alias suchen
 */
public static function umbenennenzuAliasname(string|array $Wert, string $Sprache = 'de-DE'): array|string
```
## loeschenSonderzeichen
```
/**
 * Löscht alle Sonderzeichen aus einem String oder Array und gibt die Daten so zurück.
 * Zahlen, äöü und ß zählen nicht als Sonderzeichen.
 *
 * @param $Wert array|string Die zu reinigen Werte.
 *
 * @return array|string|null
 *
 * @since 0.0.8
 */
 public static function loeschenSonderzeichen($Wert): array|string|null
 ```
## UTF8 zu RTF
```
/**
 * Bereitet die einen UTF8 Code zur Ausgabe im rtf-Format vor.
 * 
 * @param   string  $text
 *
 * @return string
 *
 * @since 0.5.0
 */
public static function utf8_zu_rtf(string $text): string
```
## Zahl2Text
```
/**
 * Gibt die Zahlen als Buchstabenfolge zurück.
 *
 * @param   string|int  $Wert
 *
 * @return string|bool
 *
 * @since 0.3.0
 */
public static function Zahl2Text(string|int $Wert): string|bool
```
# Layout Hilfen
Werden in der Klasse GMF_Layout bereitgestellt.

## anpassenLink
````
/**
 * Löscht oder fügt bestimmte Parameter ein. Es werden komplette, absolute Links zurückgegeben.
 *
 * @param   array   $EntfernenParameter  Der zu löschende Parameter als Array.
 * @param   array   $NeueParameter       Parameter mit dem Wert (z.B. Abfrage=313212), die neu hinzugefügt werden
 *                                       sollen als Array.
 * @param   string  $Fragment            Der Ankertext ohne #-Zeichen.
 *
 * @return string
 * @since   0.4.0
 */
public static function anpassenLink(array $EntfernenParameter, array $NeueParameter = array(), string $Fragment = '')
````

## ermittleHauptfarbeRBG
```
/**
 * Gibt die RGB-Werte der passenden Farbe als String zurück (z.B. "80,25,90").
 * 
 * @param   string  $pfad  Absoluter Datenpfad des Bildes
 *
 * @return string
 * @since 0.0.6
 */
public static function ermittleHauptfarbeRBG(string $pfad) : string {
```
## erstelleAuswahlfeld
```
/**
 * Gibt ein Auswahlformular (Select) zurück.
 *
 * @param   string       $Titel             Das Label, bzw. die Beschreibung
 * @param   string       $id                Das ID-Tag, welches zugleich auch der Name, bzw. Value der Variable ist
 * @param   array        $Auswahltopf       Welche Begriffe zur Auswahl bereitstehen?
 * @param   string|null  $Vorauswahl        Welche Begriffe sind im aktuellen Formular schon gewählt?
 * @param   string       $Methode           Welche Methode zu Übermittlung angewandt wird. Standard ist GET.
 * @param   bool         $multiple          Soll das Formular eine Mehrfachauswahl zulassen? Standard ist true.
 * @param   mixed        $action            Die Aktion, die beim Absenden ausgeführt werden soll
 * @param   string       $Gruppe            Mit diesem Parameter kann zusätzlich eine Gruppe festgelegt werden, welche alles automatisch erstellen, Formulare zusammenfasst.
 * @param   array        $Formularfelder    Zusätzliche Formularfelder (siehe erstelleFormularfelder).
 * @param   string       $class             CSS Klasse, z. B. js-example-basic-multiple
 *
 * @return string
 *
 * @todo  Custom CSS Möglichkeiten hinzufügen oder entfernen -> ist eigentlich Sache des Templates
 *
 * @since 0.0.8
 */
public static function erstelleAuswahlfeld(string $Titel, string $id, array $Auswahltopf,string|null $Vorauswahl = '', string $Methode = 'get', bool $multiple = true, mixed $action = false, string $Gruppe = ''): string
```
## erstelleDetails
```
/**
 * Gibt eine Datalist zurück.
 *
 * @param   string  $ID             Die Listen-ID (list="$ID")
 * @param   array   $Auswahltopf    Worte, welche als Option erscheinen sollen.
 * @param   bool    $Java           Soll die Datalist mit Javascript betrieben werden.
 *
 * @return string
 *
 * @link https://dev.to/siddev/customise-datalist-45p0 für Javascirpt nach diesem Prinzip
 *
 * @since 0.0.8
 * @Version 0.0.9
 */
public static function erstelleDatalist(string $ID, array $Auswahltopf, bool $Java = false) : string
```
## erstelleDetails
```	
/**
 * Erstellt einen Details-Bereich in HTML.
 *
 * @param   string  $Titel
 * @param   string  $Inhalt
 *
 * @return string
 *
 * @Link https://www.w3schools.com/tags/tag_details.asp
 * @since 0.0.6
 */
public static function erstelleDetails(string $Titel, string $Inhalt) : string {
```
## erstelleFormularfelder
```
/**
 * Erstellt einfache Formularfelder.
 * 
 * @param   array  $Daten   Ein Mehrdimensionales Array mit den wichtigsten Informationen.
 *                          z.B. array( 0 =>
 *                                array( 'Titel' => "Testformular",
 *                                      'Name'   => "Test",
 *                                      'Type'  => "hidden",
 *                                      'Value'  => "")
 *                                 );
 *
 * @return string
 *
 * @since 0.0.9
 */
```
## erstelleLink
```
/**
* Erstellt einen einfachen Link. Damit wird sichergestellt, dass die Qualität stimmt.
*
* @param string $Link Die Zieladresse
* @param string $Text Der angezeigte Link
* @param string $Title Der Titel, wenn nicht angezeigt entspricht er dem angezeigten Link. Alle " werden zu ' umgestellt, damit der Link korrekt ausgegeben wird.
* @param string $Target Standardmässig wird der Link im selben Fenster geöffnet, ansonsten muss '_blank' angegeben werden.
* @param string $Style Einen benutzerdefinierten Style.
* @param string $Klasse Die CSS Klasse des Links.
* @param string $Rel Hier kann man z.B. ein 'nofollow' Tag anhängen.
* @return string
* @since 0.0.1
*/
public static function erstelleLink(string $Link, string $Text,string $Title = '',string $Target = '_self',string $Style = '',string $Klasse = '',string $Rel = '') {
```
## formatiereDatum
 ```
/**
* Formatiert ein beliebiges Datum.
*
* @param string $Start Das (Start)Datum.
* @param string $Ende Das Enddatum (optional).
* @param boolean $Tage Wenn "true" werden das Datum mit Wochentagsnamen ergänzt (optional).
* @param boolean $Monate Wenn "true" werden die Monate mit Namen wiedergegeben (optional).
* @since  0.0.2
* @return string Mit dem formatierten Datum
*/
public static function formatiereDatum(string $Start,string $Ende = '',bool $Tage = false, bool $Monate = false)
 ```
## istMobil
Prüft, ob ein Mobilgerät auf die Webseite zugreift. So kann für Desktop oder Smartphone die Funktion entsprechend gestaltet werden.

# Gratia-Mira Hilfen
Ist die Bibel-Vers-Anzeige installiert, wird automatisch die Klasse "GMF_GratiaBruecke" und die "Tools" des Moduls geladen. Diese Funktionen stehen nur auf gratia-mira.ch zur Verfügung.

# Sicherheit Hilfen
Sicherheitsfunktionen.

## ersetzeParameterHistory
```
/**
* Ersetzt den aktuellen History Eintrag. 
* So kann verhindert werden, dass Variablen doppelt übermittelt werden.
*
* @return string
* @link https://developer.mozilla.org/en-US/docs/Web/API/History/replaceState
* @since 0.0.9
*/
public static function ersetzeParameterHistory() : string {
```
## generierePasswort
```
/**
 * Generiert ein sicheres Passwort mit 10 - 20 Zeichen.
 *
 * @param   string  $Text Text, aus dem das Passwort generiert wird.
 * @return string
 *
 * @since 0.2.0
 * @versin 0.2.1
 */
public static function generierePasswort(string $Text): string
```

## initialisieFormularsicherheit
Initialisiert eine Sicherheitskontrolle gegen Bots und Duplikate, welche unabhängig von Drittanbietern ist.

## pruefeFormularsicherheit
```
/**
* Wertet die Formular-Eingabe der Funktion "initialisieFormularsicherheit" aus.
*
* @param boolean $Duplikat Soll die Eingabe auf ein Duplikat geprüft werden?
* @param array $Eingabe Array mit allen Parameter-Inhalten, welche geprüft werden sollen.
* @param boolean $Honeypot
* @since 0.0.3
* @return boolean
*/
public static function pruefeFormularsicherheit(bool $Duplikat = true , array $Eingabe = array(),bool $Honeypot = true) {
```

## initialisieReCaptcha
Initialisiert ReCaptcha invisible. Die Key werden vom Joomla-Plugin übernommen. Unterstützt wird auch das Linksbündige Layout. Beides muss über das System-Plugin von Joomla konfiguriert werden. Die Funktion muss im <form>-Tag aufgerufen werden.

## pruefeReCaptcha
```
/**
 * Prüft die ReCaptcha-Antwort und gibt true oder false zurück.
 * @var int $Score Über diesen Parameter kann gesteuert werden, wie hoch der Score sein soll. 
 * @return boolean Der Score muss 0.3 oder höher sein, damit true erreicht wird.
 * @since 0.0.1
 * @version 0.0.3
 */
public static function pruefeReCaptcha($Score = 0.3) {
```
# Bibliotheken
## ForceUTF8
Ist das Framework geladen, kann diese Klasse mit folgendem Code eingebunden werden.
 ```
use ForceUTF8\Encoding;

$utf8_string = Encoding::fixUTF8($garbled_utf8_string);
 ```
Mehr Informationen unter: https://github.com/neitanod/forceutf8/blob/master/README.md

# Für Entwickler
## Module
Ab Version 0.0.2 bei Modulen, die auf das Framework zugreifen folgenden Code an den Anfang des Entry-Points der Erweiterung stellen.
 ```
$GMF_Fehler = false;
if(!defined('GM_FRAMEWORK') or version_compare(constant('GM_FRAMEWORK'), '{Benötigte Version}') < 0) {
    $GMF_Fehler = true;
    goto GMF_Fehler;
}
 ```
 Und am Ende der Files:
 ```
GMF_Fehler:
if($GMF_Fehler) {
    require JModuleHelper::getLayoutPath('{mod_modulname}','gmf_error'); 
}
 ```
 Weiter im Ordner tmpl des Moduls ein Dokument mit dem Namen "gmf_error.php" erstellen, mit folgendem Inhalt.
 ```
<?php
defined('_JEXEC') or die;

/**
 * Fehlerausgabe Framework
 * 
 * @since 0.0.4 (Framework)
 */

if(defined('GM_FRAMEWORK')) {
    $Aufgabe = 'aktualisieren';
} else {
    $Aufgabe = 'installieren oder <a href="https://docs.joomla.org/Help4.x:Plugins:_Name_of_Plugin/de" title="zur Joomla-Hilfeseite" target="_blank">aktivieren</a>';
}
?>
Bitte <a href="https://gitlab.com/gratia-mira-opensource/gratia-mira-framework/-/releases" title="zum Framework" target="_blank">GMFramework</a> <?php echo $Aufgabe; ?>!
 ```
## Komponenten
In der `ServiceProviderInterface` Klasse in der funktion `register()` folgenden Code einfügen. Die Abhängigkeit wird somit im Frond- und Backend geprüft.
 ```
if(!defined('GM_FRAMEWORK') or version_compare(constant('GM_FRAMEWORK'), '{Version}') < 0) {
    if(defined('GM_FRAMEWORK')) {
        $Aufgabe = 'aktualisieren';
    } else {
        $Aufgabe = 'installieren oder <a href="https://docs.joomla.org/Help4.x:Plugins:_Name_of_Plugin/de" title="zur Joomla-Hilfeseite" target="_blank">aktivieren</a>';
    }

    exit('Bitte <a href="https://gitlab.com/gratia-mira-opensource/gratia-mira-framework/-/releases" title="zum Framework" target="_blank">GMFramework </a>' .  $Aufgabe . '!');
}
 ```