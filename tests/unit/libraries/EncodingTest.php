<?php
/**
 * @package     libraries
 * @subpackage
 *
 * @copyright   A copyright
 * @license     A "Slug" license name e.g. GPL2
 */

namespace libraries;

require_once '../../../source/libraries/forceutf8.php';

use ForceUTF8\Encoding;
use PHPUnit\Framework\TestCase;

class EncodingTest extends TestCase
{
 function testforceuf8()
 {
	    $text = Encoding::fixUTF8plus('FÃÂÂÂÂ©dÃÂÂÂÂ©ration Camerounaise de Football\n');

		$this->assertStringContainsString('Federation Camerounaise de Football',$text);

 }
	function testforceuf8e() {
		$text = Encoding::fixUTF8plus('<span+class%3D"teaser__title">Cortège+mit+Wetterglück+und+rosa-grünen+Farbtupfern<%2Fspan>');

		$this->assertStringContainsString('<span+class%3D"teaser__title">Cortege+mit+Wetterglück+und+rosa-grünen+Farbtupfern<%2Fspan>',$text);
	}
	function testforceuf8o() {
		$text = Encoding::fixUTF8plus('lyòö');

		$this->assertStringContainsString('lyoö',$text);
	}

	function testforceuf8nbsp() {
		$text = Encoding::fixUTF8plus('lyòö 5');

		$this->assertStringContainsString('lyoö 5',$text);
	}

	function testforceuf8MehrereLeerzeichen() {
		$text = Encoding::fixUTF8plus('lyòö   5');

		$this->assertStringContainsString('lyoö 5',$text);
	}
}


