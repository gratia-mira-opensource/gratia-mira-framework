<?php
/**
 * @package     framework
 * @subpackage
 *
 * @copyright   A copyright
 * @license     A "Slug" license name e.g. GPL2
 */
const _JEXEC = 1;
require_once '../../../source/framework/gmf_allgemein.php';

use PHPUnit\Framework\TestCase;

class TestZahl2Text extends TestCase
{

	public function testZahl2textInt2()
	{
		$Zahlenstring = \GMF_Allgemein::Zahl2Text(33);
		$this->assertEquals($Zahlenstring,'dreiunddreißig');

	}

	public function testZahl2textInt3()
	{
		$Zahlenstring = \GMF_Allgemein::Zahl2Text(100);
		$this->assertEquals($Zahlenstring,'hundert');

	}
	public function testZahl2textInt6()
	{
		$Zahlenstring = \GMF_Allgemein::Zahl2Text(100000);
		$this->assertEquals($Zahlenstring,'hunderttausend');

	}
	public function testZahl2textInt7()
	{
		$Zahlenstring = \GMF_Allgemein::Zahl2Text(1000000);
		$this->assertEquals($Zahlenstring,'eine Million');

	}

	public function testZahl2textStringLeerschlag()
	{
		$Zahlenstring = \GMF_Allgemein::zahl2text('12 232');
		$this->assertEquals($Zahlenstring,'zwölftausend zweihundertzweiunddreißig');

	}

	public function testZahl2textStringTausendzeichen()
	{
		$Zahlenstring = \GMF_Allgemein::zahl2text("12'232");
		$this->assertEquals($Zahlenstring,'zwölftausend zweihundertzweiunddreißig');

	}

	public function testZahl2textStringLeerschlagHinten()
	{
		$Zahlenstring = \GMF_Allgemein::zahl2text('12 ');
		$this->assertEquals($Zahlenstring,'zwölf');

	}

	public function testZahl2textStringTausendLeerschlag()
	{
		$Zahlenstring = \GMF_Allgemein::zahl2text('1 000');
		$this->assertEquals($Zahlenstring,'tausend');

	}

	public function testZahl2textStringZehntausendLeerschlag()
	{
		$Zahlenstring = \GMF_Allgemein::zahl2text('10 000');
		$this->assertEquals($Zahlenstring,'zehntausend');

	}
}
