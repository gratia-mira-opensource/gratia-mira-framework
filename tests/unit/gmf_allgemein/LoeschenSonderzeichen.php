<?php
/**
 * @package     ${NAMESPACE}
 * @subpackage
 *
 * @copyright   A copyright
 * @license     A "Slug" license name e.g. GPL2
 */

const _JEXEC = 1;
require_once '../../../source/framework/gmf_allgemein.php';

use PHPUnit\Framework\TestCase;


class LoeschenSonderzeichen extends TestCase
{

	public function erstelleZitat($Wert) {
		return '»' . $Wert . '«';
	}

	public function testLoeschenSonderzeichenString()
	{
		$Texte =  [
			'redet-durch:die-kleinen-propheten'                        => 'redet durch die kleinen propheten',
			'Und Er sprach: Fiat Lux!'                                 => 'Und Er sprach Fiat Lux',
			'Möge unser HERR kommen, daß wir äusserst glücklich sind ' => 'Möge unser HERR kommen daß wir äusserst glücklich sind',
			''                                                         => '',
			'Ja – Ja'                                                  => 'Ja Ja',
			'Ja –- Nein'                                               => 'Ja Nein',
		];

		foreach ($Texte as $Text => $Ziel) {
			$Korrektur = \GMF_Allgemein::loeschenSonderzeichen($Text);
			$this->assertEquals($Ziel,$Korrektur,$this->erstelleZitat($Korrektur) . ' ist nicht gleich ' . $this->erstelleZitat($Ziel));
		}

	}
	public function testLoeschenSonderzeichenArray()
	{
		$Array =  [
			'redet durch die kleinen propheten'                      => 'redet-durch-die-kleinen-propheten',
			'Und Er sprach Fiat Lux'                                 => 'Und Er sprach: Fiat Lux!',
			'Möge unser HERR kommen daß wir äusserst glücklich sind' => 'Möge unser HERR kommen, daß wir äusserst glücklich sind ',
			''                                                       => '',
		];

		$ZielArray =  [
			'redet durch die kleinen propheten'                      => 'redet durch die kleinen propheten',
			'Und Er sprach Fiat Lux'                                 => 'Und Er sprach Fiat Lux',
			'Möge unser HERR kommen daß wir äusserst glücklich sind' => 'Möge unser HERR kommen daß wir äusserst glücklich sind',
			''                                                       => '',
		];


			$Korrektur = \GMF_Allgemein::loeschenSonderzeichen($Array);
			$this->assertEquals($ZielArray,$Korrektur,'Array ist nicht gleich');

	}
}
