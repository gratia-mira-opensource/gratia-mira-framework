<?php

const _JEXEC = 1;
require_once '../../../source/framework/gmf_layout.php';

use PHPUnit\Framework\TestCase;

class ErstelleFormularfelder extends TestCase
{
	public function testerstelleFormularfelderGrundfunktion()
	{
		// Hidden: Label ausblenden
		$Array[0] = array('Titel' => "Testformular",
		                  'Name'  => "Test",
		                  'Type'  => "hidden",
		                  'Value' => "");
		// Grundfunktion: Label anzeigen
		$Array[1] = array('Titel' => "Testfrage",
		                  'Name'  => "Frage",
		                  'Type'  => "text",
		                  'Value' => "DANKE!");
		// Doppelarray
		$Array[2] = array(0 => ['Titel' => "Testformular",
		                        'Name'  => "Test",
		                        'Type'  => "hidden",
		                        'Value' => ""],
		                  1 => ['Titel' => "Testfrage",
		                        'Name'  => "Frage",
		                        'Type'  => "text",
		                        'Value' => "DANKE!"]);


		$Fertig[0] = '<input type="hidden" id="Test" name="Test" value=""/>';
		$Fertig[1] = '<label for="Frage">Testfrage</label><br><input type="text" id="Frage" name="Frage" value="DANKE!"/>';
		$Fertig[2] = '<input type="hidden" id="Test" name="Test" value=""/><label for="Frage">Testfrage</label><br><input type="text" id="Frage" name="Frage" value="DANKE!"/>';


		for ($i = 0; $i < count($Array); $i++)
		{
			$Formular = \GMF_Layout::erstelleFormularfelder($Array[$i]);

			$this->assertEquals($Fertig[$i], $Formular, 'Aktuelles Resultat: ' . $Formular);
		}
	}
}
