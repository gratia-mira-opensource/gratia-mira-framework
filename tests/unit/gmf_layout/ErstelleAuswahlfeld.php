<?php
/**
 * @package     ${NAMESPACE}
 * @subpackage
 *
 * @copyright   A copyright
 * @license     A "Slug" license name e.g. GPL2
 */

const _JEXEC = 1;
require_once '../../../source/framework/gmf_layout.php';

use PHPUnit\Framework\TestCase;

class ErstelleAuswahlfeld extends TestCase
{

	public function testErstelleAuswahlfeldGrundfunktion()
	{
		// Grundfunktion
		$Array[0] = array('Titel'      => "Testformular",
		                  'Id'         => 'Dankbar',
		                  'Auswahl'    => array('Tuchband', 'Jasmin', 'Gottes Dienerin', 'Gott'),
		                  'Vorauswahl' => array('Jasmin', 'Gott'));
		// Vorauswahl als String
		$Array[1] = array('Titel'      => "Testfrage",
		                  'Id'         => 'Zug-Verbindung',
		                  'Auswahl'    => array('Neunkirchen', 'Siegburg', 'DANKE'),
		                  'Vorauswahl' => "DANKE");

		$Fertig[0] =    '<form method="get" ><label for="Dankbar">Testformular</label><br><select name="Dankbar[]" id="Dankbar" style="height: auto;" multiple><option value="Tuchband">Tuchband</option><option value="Jasmin" selected>Jasmin</option><option value="Gottes Dienerin">Gottes Dienerin</option><option value="Gott" selected>Gott</option></select><input type="hidden" name="GratiaAuswahlfeld" value="Dankbar"><input type="hidden" name="GratiaAnkerposition" value="Dankbar"/><input type="submit"></form>';
		$Fertig[1] =    '<form method="get" ><label for="Zug-Verbindung">Testfrage</label><br><select name="Zug-Verbindung[]" id="Zug-Verbindung" style="height: auto;" multiple><option value="Neunkirchen">Neunkirchen</option><option value="Siegburg">Siegburg</option><option value="DANKE" selected>DANKE</option></select><input type="hidden" name="GratiaAuswahlfeld" value="Zug-Verbindung"><input type="hidden" name="GratiaAnkerposition" value="Zug-Verbindung"/><input type="submit"></form>';


		for ($i = 0; $i < count($Array); $i++)
		{
			$Formular = \GMF_Layout::erstelleAuswahlfeld($Array[$i]['Titel'], $Array[$i]['Id'], $Array[$i]['Auswahl'], $Array[$i]['Vorauswahl'], 'get');

			$Fertig[$i] = preg_replace('/\s+/','',$Fertig[$i]);
			$Formular = preg_replace('/\s+/','',$Formular);

			$this->assertEquals($Fertig[$i], $Formular, 'Aktuelles Resultat: ' . $Formular);
		}
	}
}
