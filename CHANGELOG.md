# 0.5.1 18.02.2025

* Verbesserung: Bei der Boterkennung kann zusätzlich alle Seitenaufrufe als Bot eingestuft werden, welche nicht von
  einer internen Seite kommen.

# 0.5.0 13.01.2025

* Neue Funktion: uft8_zu_rtf in der GMF_Allgemein Klasse.

# 0.4.0 26.12.2024

* Neue Funktion: Links verändern, ausgehend von der aktuellen Seite.
* Verbesserung: Auswahlfeld wird flexibler.
* Verbesserung: Tooltip wird korrekt ausgegeben, auch wenn der Linktext HTML enthält.

# 0.3.3 23.11.2024

* Verbesserung: Übernimmt das Stichwort im Passwort, wenn es nur ein Wort ist.
* Verbesserung: Auswahlfeld kann Klasse verwenden.

# 0.3.2 12.11.2024

* Verbesserung: Vorauswahl funktioniert nun genauer.
* Fehler: Einzelne Formulare wurden nicht korrekt ausgegeben.

# 0.3.1 14.10.2024

* Verbesserung: Boterkennung, wenn die $_SERVER nicht gesetzt ist, ergibt keine Warnung!
* Verbesserung: Boterkennung Update.

# 0.3.0 24.08.2024

* Neue Funktion: Zahl können zu entsprechenden Wörtern umgewandelt werden.

# 0.2.0 04.05.2024

* Neue Funktion: Generiere sichere Passwortvorschläge.

# 0.1.0 30.04.2023

* Neue Funktion: Prüft, ob der Admin angemeldet ist.
* Neue Klasse: Forceutf8 https://github.com/neitanod/forceutf8/blob/master/README.md

# 0.0.9 08.04.2023

* Verbesserung: Zusätzliche Formularfelder in erstelleAuswahlformular.
* Neue Funktion: erstelleFormularfelder.
* Neue Funktion: Skript History ersetzen.
* Neue Funktion: Botzugriffe erkennen.
* Neue Funktion: Admin erkennen.

# 0.0.8 25.11.2023

* Neue Funktion: Auswahlfelder dynamisch erstellen.
* Neue Funktion: Sonderzeichen aus String oder Array löschen.

# 0.0.7 16.10.2023

* Verbesserung: Alias Funktion überarbeitet.

# 0.0.6 14.10.2023

* Neu: Lädt die Bibelvers-Konstanten automatisch, wenn das Plugin auf Gratia-Mira läuft.
* Neu: Funktion, die die Hauptfarbe ermittelt eines jpg Bildes ermittelt.
* Neu: Funktion, die eine HTML-Details-Bereich erstellt.
* Fehler: Fehler wenn prüfeFormularsicherheit Duplikate auf false.

# 0.0.5 15.06.2023

* Fehler: Honigpot-Feld wurde angezeigt.

# 0.0.4 18.05.2023

* Neue Funktion: Rukovoditel API Schnittstelle (im Moment nur für Stiftung Edition Nehemia).
* Fehler: Datumsansicht mit Tagen funktioniert korrekt.

# 0.0.3 15.05.2023

* Neue Funktion: Es gibt nun eine Google ReCaptcha Alternative.
* Verbesserung: Score von Google ReCaptcha muss wesentlich weniger hoch sein.

# 0.0.2 04.05.2023

* Verbesserung: Besseres Error-Handling, wenn Framework nicht installiert oder veraltet ist.
* Neue Funktion: Datum formatieren.
  0.0.1 18.04.2023
  Erstes offizielles Release.