<?php

// Kein direkter Aufruf der PHP-Datei
defined('_JEXEC') or die('Restricted Access');


use Joomla\CMS\Factory;
use Joomla\CMS\Filter\OutputFilter;

class GMF_Allgemein
{

	/**
	 * Prüft, ob ein Admin angemeldet ist.
	 *
	 * @return bool
	 * @since 0.1.0
	 */
	public static function istAdmin(): bool
	{
		try
		{
			$user = Factory::getApplication()->getIdentity();

			return $user->authorise('core.admin');
		}
		catch (Exception $e)
		{
			return false;
		}
	}

	/**
	 * Prüft, ob ein Bot oder ein Benutzer auf die Seite zugreift.
	 *
	 * @param   bool  $NurIntern Alle externen Benutzer und Direktzugriffe als Bot einstufen.
	 *
	 * @return bool
	 * @link    https://stackoverflow.com/questions/677419/how-to-detect-search-engine-bots-with-php
	 * @since   0.0.9
	 * @version 0.5.1
	 */
	public static function istBot(bool $NurIntern = false): bool
	{
		if($NurIntern) {
			if(!isset($_SERVER['HTTP_REFERER']) and !str_contains($_SERVER['HTTP_REFERER'], str_replace('www.','',$_SERVER['SERVER_NAME']))) {
				return true;
			}
		}

		return isset($_SERVER['HTTP_USER_AGENT']) and preg_match('/bot|crawl|slurp|spider|mediapartners|googleother/i', $_SERVER['HTTP_USER_AGENT']);
	}

	/**
	 * @param   string|array  $Wert
	 * @param   string        $Sprache
	 *
	 * @return array|string
	 * @deprecated 0.0.6 benutze stattdessen "umbenennenzuAliasname()"
	 *
	 * @since      0.0.1
	 */
	public static function unbenennenzuAliasname(string|array $Wert, string $Sprache = 'de-DE'): array|string
	{
		return GMF_Allgemein::umbenennenzuAliasname($Wert, $Sprache);
	}

	/**
	 * Verändert ein String oder den Text einen einfachen Array in Aliasgerechte Namen. Im Moment werden alle Sonderzeichen mit Ausnahme von ä, ö, ü einfach weggelöscht.
	 *
	 * @param   string|array  $Wert  Die Daten welche zu Aliasnamen verändert werden sollen
	 * @param   string        $Sprache
	 *
	 * @return string|array
	 * @since   0.0.1
	 * @version 0.0.7
	 *
	 * @todo    Standard Joomla-Funktion für Alias suchen
	 */
	public static function umbenennenzuAliasname(string|array $Wert, string $Sprache = 'de-DE'): array|string
	{

		if (is_array($Wert))
		{
			$Alias = array();
			foreach ($Wert as $Alias)
			{
				$Alias[] = OutputFilter::stringUrlSafe($Alias, $Sprache);
			}
		}
		else
		{
			$Alias = OutputFilter::stringUrlSafe($Wert, $Sprache);
		}

		return preg_replace(array('/[^a-z0-9-]/', '/-+/'), array('', '-'), $Alias);
	}

	/**
	 * Löscht alle Sonderzeichen aus einem String oder Array und gibt die Daten so zurück.
	 * Zahlen, äöü und ß zählen nicht als Sonderzeichen.
	 *
	 * @param $Wert array|string Die zu reinigen Werte.
	 *
	 * @return array|string|null
	 *
	 * @since 0.0.8
	 */
	public static function loeschenSonderzeichen(array|string $Wert): array|string|null
	{
		//Sonderzeichen wegfiltern
		$Wert = preg_replace('/[^a-zöüäß0-9]/ui', ' ', $Wert);

		// Doppelte Leerschläge ersetzen, sowie trimmen
		return preg_replace(array('/\s+/', '/^\s/', '/\s$/'), array(' ', ''), $Wert);
	}

	/**
	 * Bereitet die einen UTF8 Code zur Ausgabe im rtf-Format vor.
	 *
	 * @param   string  $text
	 *
	 * @return string
	 *
	 * @since 0.5.0
	 */
	public static function utf8_zu_rtf(string $text): string
	{
		$output = '';
		for ($i = 0; $i < mb_strlen($text, 'UTF-8'); $i++)
		{
			$char      = mb_substr($text, $i, 1, 'UTF-8');
			$codepoint = mb_ord($char, 'UTF-8'); // Unicode-Wert des Zeichens
			if ($codepoint <= 127)
			{
				// ASCII-Zeichen direkt verwenden
				$output .= $char;
			}
			else
			{
				// Unicode-Zeichen in RTF-Format umwandeln
				$output .= sprintf('\\u%d?', $codepoint);
			}
		}

		return $output;
	}

	/**
	 * Gibt die Zahlen als Buchstabenfolge zurück.
	 *
	 * @param   string|int  $Wert
	 *
	 * @return string|bool
	 *
	 * @since 0.3.0
	 */
	public static function Zahl2Text(string|int $Wert): string|bool
	{
		$Zahl = intval(preg_replace('/\D+/', '', $Wert));
		if ($Zahl > 0)
		{
			require_once __DIR__ . '/../libraries/zahl2text.php';

			$converter = new Zahl2Text();

			return $converter->convert($Zahl);
		}
		else
		{
			return false;
		}
	}

}
