<?php

// Kein direkter Aufruf der PHP-Datei
defined('_JEXEC') or die('Restricted Access');


class GMF_GratiaBruecke {

	/**
	 * Initialisiert die BenutzereingabeAnalysator-Klasse und gibt ein Objekt mit den verarbeiteten Daten zurück. Voraussetzung ist natürlich, dass die gleichen Variabel-Namen für die Anfrage (Request) verwendet werden.
	 *
	 * @param boolean $ErstbesuchKeineParameter Sollen beim ersten Besuch die Parameter nicht berücksichtigt werden.
	 * @param string $AdminStichwort Soll ein Admin Stichwort die Auswertung beeinflussen?
	 * @param integer $NeuladenZeit Nach welcher Zeit soll die Seite neu geladen werden (funktioniert nur im Zusammenspiel mit einer Bibelversanzeigen).
	 * @param string $AdminLayout Hier könnte das Layout übersteuert werden (funktioniert nur im Zusammenspiel mit einer Bibelversanzeigen).
	 * @return object Die analysierten Eingabedaten des Benutzers
	 * @since 0.0.1
	 */
	public static function analysiereEingabe(bool $ErstbesuchKeineParameter = false,string $AdminStichwort = '',int $NeuladenZeit = 0,string $AdminLayout = 'Studienansicht') {

		if(defined('GM_MOD_BIBELVERSANZEIGE')) {
			$obj = new stdClass();
			// Parameter für BenutzerEingabeAnalysator Klasse
			$obj->ErstbesuchKeineParameter = $ErstbesuchKeineParameter; // Modus tritt nicht in Kraft
			$obj->AdminStichwort = $AdminStichwort; // Muss definiert sein
			$obj->NeuladenZeit = $NeuladenZeit; // Muss definiert sein
			$obj->AdminLayout = $AdminLayout; // Muss definiert sein

			// Klasse aufrufen
			return new \BenutzerEingabeAnalysator($obj);
		}
	}

	/**
	 * Erstellt eine API Abfrage und gibt ein Array der Daten zurück.
	 * Die IP-Adresse muss für den Zugriff freigegeben sein!
	 *
	 * @param array $Parameter Alle Parameter ab "action" in einem Array.
	 * @return array Die Daten von Rukovodititel.
	 * @since 0.0.4
	 * @todo Dateiname als Einstellung oder Parameter Sicherheit?
	 */
	public static function holeRukoDaten(array $Parameter) {

		$LoginVerzeichnis = preg_replace('/[a-z]+$/i','',$_SERVER['DOCUMENT_ROOT']);

		$LoginDaten = json_decode(file_get_contents($LoginVerzeichnis . 'login_ruko_holidays.json'));

		if($LoginDaten) {
	
			$Login = [
				'key' => trim($LoginDaten->key),  
				'username' => trim($LoginDaten->user),
				'password' => trim($LoginDaten->pass),
			];

			$params = array_merge($Login,$Parameter);
			
			
			$ch = curl_init(trim($LoginDaten->url)); //API Url
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_TIMEOUT, 10);			
			$result = curl_exec($ch);
			curl_close($ch);
			
			if($result)
			{
			$result = json_decode($result,true);
			
			// print_r($result['data']);
			return $result['data'];
			}
		}

	}

}
?>
