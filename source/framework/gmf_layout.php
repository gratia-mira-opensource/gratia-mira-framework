<?php

// Kein direkter Aufruf der PHP-Datei
defined('_JEXEC') or die('Restricted Access');


class GMF_Layout
{

	/**
	 * Löscht oder fügt bestimmte Parameter ein. Es werden komplette, absolute Links zurückgegeben.
	 *
	 * @param   array   $EntfernenParameter  Der zu löschende Parameter als Array.
	 * @param   array   $NeueParameter       Parameter mit dem Wert (z.B. Abfrage=313212), die neu hinzugefügt werden
	 *                                       sollen als Array.
	 * @param   string  $Fragment            Der Ankertext ohne #-Zeichen.
	 *
	 * @return string
	 * @since   0.4.0
	 */
	public static function anpassenLink(array $EntfernenParameter, array $NeueParameter = array(), string $Fragment = '')
	{

		$LinkArray = parse_url($_SERVER['REQUEST_URI']);

		// Pfad
		if (isset($LinkArray['path']))
		{
			$LinkArray['path'] = str_replace("/" . Installationsverzeichnis, '', $LinkArray['path']);
		}
		else
		{
			$LinkArray['path'] = '';
		}

		// Parameter löschen
		if (isset($LinkArray['query']) and $EntfernenParameter[0] !== 'alle')
		{
			foreach ($EntfernenParameter as $Param)
			{
				$LinkArray['query'] = preg_replace('/(' . $Param . '=.*)(\&|$|\#)/Ui', '$2', $LinkArray['query']);
			}
			$LinkArray['query'] = '?' . preg_replace(array('/&+/', '/^&/', '/&$/'), array('&', ''), $LinkArray['query']);
		}
		else
		{
			$LinkArray['query'] = '';
		}

		// Neue Parameter
		if ($NeueParameter)
		{
			foreach ($NeueParameter as $NeuParameter)
			{
				if (empty($LinkArray['query']))
				{
					$Vorzeichen = '?';
				}
				else
				{
					$Vorzeichen = '&';
				}

				$LinkArray['query'] .= $Vorzeichen . $NeuParameter;
			}
		}
		// Anker (funktioniert leider im Moment nicht - https://stackoverflow.com/questions/2317508/get-fragment-value-after-hash-from-a-url)
		if ($Fragment)
		{
			$LinkArray['fragment'] = '#' . $Fragment;
		}
		elseif (isset($LinkArray['fragment']))
		{
			$LinkArray['fragment'] = '#' . $LinkArray['fragment'];
		}
		else
		{
			$LinkArray['fragment'] = '';
		}

		return JUri . $LinkArray['path'] . $LinkArray['query'] . $LinkArray['fragment'];
	}

	/**
	 * Gibt die RGB-Werte der passenden Farbe als String zurück (z.B. "80,25,90").
	 *
	 * @param   string  $pfad        Absoluter Datenpfad des Bildes
	 * @param   int     $Kontrast    Der Wert, der die R+G+B Werte zusammengezählt nicht überschreiten darf.
	 * @param   int     $Farbigkeit  Der Wert, welcher nicht unterschreiten werden darf.
	 *
	 * @return string
	 * @since 0.0.6
	 */
	public static function ermittleHauptfarbeRBG(string $pfad, int $Kontrast = 500, int $Farbigkeit = 120): string
	{
		if (str_contains($pfad, '.jp'))
		{
			$img       = imagecreatefromjpeg($pfad);
			$imgSize   = GetImageSize($pfad);
			$imgWidth  = $imgSize[0];
			$imgHeight = $imgSize[1];

			//Farben zählen
			$colors = array();
			for ($x = 0; $x < $imgWidth; $x++)
			{
				for ($y = 0; $y < $imgHeight; $y++)
				{
					//$colors ergibt ein Array mit dem imagecolor als Index und dem Zähler als Wert
					$colors[imagecolorat($img, $x, $y)]++;
				}
			}
			// Sortieren, damit die Farbe mit den meisten Punkten zuerst kommt
			arsort($colors);

			// Sicherstellen, dass keine zu helle, bzw. grelle Farbe als Hauptfarbe festgelegt wird
			$z = 0;
			do
			{
				$Keys = array_keys($colors);

				$Index = $Keys[$z];
				$RGB   = imagecolorsforindex($img, $Index);

				$Total = $RGB['red'] + $RGB['green'] + $RGB['blue'];

				$FarbigkeitTest = abs($RGB['red'] - $RGB['green']) + abs($RGB['red'] - $RGB['blue']) + abs($RGB['green'] - $RGB['blue']);

				$z++;

			} while (($Total > $Kontrast or $FarbigkeitTest < $Farbigkeit) and count($colors) > $z);


			return $RGB['red'] . ',' . $RGB['green'] . ',' . $RGB['blue'];

		}
		else
		{
			return '80,80,80';
		}
	}

	/**
	 * Gibt ein Auswahlformular (Select) zurück.
	 *
	 * @param   string       $Titel           Das Label, bzw. die Beschreibung
	 * @param   string       $id              Das ID-Tag, welches zugleich auch der Name, bzw. Value der Variable ist
	 * @param   array        $Auswahltopf     Welche Begriffe zur Auswahl bereitstehen?
	 * @param   string|null  $Vorauswahl      Welche Begriffe sind im aktuellen Formular schon gewählt?
	 * @param   string       $Methode         Welche Methode zu Übermittlung angewandt wird. Wird diese nicht leer gelassen, wird ein eigenständiges Formular zurückgegeben.
	 * @param   bool         $multiple        Soll das Formular eine Mehrfachauswahl zulassen? Standard ist true.
	 * @param   mixed        $action          Die Aktion, die beim Absenden ausgeführt werden soll
	 * @param   string       $Gruppe          Mit diesem Parameter kann zusätzlich eine Gruppe festgelegt werden, welche alles automatisch erstellen, Formulare zusammenfasst.
	 * @param   array        $Formularfelder  Zusätzliche Formularfelder (siehe erstelleFormularfelder).
	 * @param   string       $class           CSS Klasse, z. B. js-example-basic-multiple
	 *
	 * @return string
	 *
	 * @since   0.0.9
	 * @version 0.4.0
	 */
	public static function erstelleAuswahlfeld(string $Titel, string $id, array $Auswahltopf, mixed $Vorauswahl = '', string $Methode = '', bool $multiple = true, mixed $action = false, string $Gruppe = '', array $Formularfelder = array(), string $class = ''): string
	{

		$action       = $action ? 'action="' . $action . '"' : '';
		$htmlMultiple = $multiple ? 'style="height: auto;" multiple' : '';
		$name         = $multiple ? $id . '[]' : $id;

		if (is_array($Vorauswahl))
		{
			$Vorauswahl = str_replace('\|', '|', preg_quote(implode('|', $Vorauswahl)));
		}
		else
		{
			$Vorauswahl = str_replace(';', '|', strval($Vorauswahl));
		}
		// Damit sicher keine Leere Ausgabe entsteht.
		$Vorauswahl = preg_replace('/\|$/', '', $Vorauswahl);

		$htmlAuswahl = '';
		foreach ($Auswahltopf as $Auswahl)
		{
			if ($Vorauswahl and preg_match('/^(' . $Vorauswahl . ')$/u', $Auswahl))
			{
				$htmlselected = ' selected';
			}
			else
			{
				$htmlselected = '';
			}
			$htmlAuswahl .= '<option value="' . $Auswahl . '"' . $htmlselected . '>' . $Auswahl . '</option>';
		}

		if ($class)
		{
			$class = ' class="' . $class . '" ';
		}

		// Formularfelder
		$html = $Methode ? '<form method="' . $Methode . '" ' . $action . '>' : '';
		$html .= '<label for="' . $id . '">' . $Titel . '</label>';
		$html .= '<select' . $class . ' name="' . $name . '" id="' . $id . '" ' . $htmlMultiple . '>' . $htmlAuswahl . '</select>';
		$html .= $Formularfelder ? self::erstelleFormularfelder($Formularfelder) : '';
		$html .= $Gruppe ? '<input type="hidden" name="GratiaFormularGruppe" value="' . $Gruppe . '">' : '';
		$html .= $Methode ? '<input type="hidden" name="GratiaAuswahlfeld" value="' . $id . '">' : '';
		$html .= $Methode ? '<input type="hidden" name="GratiaAnkerposition" value="' . $id . '"/>' : '';
		$html .= $Methode ? '<input type="submit">' : '';
		$html .= $Methode ? '</form>' : '';

		return $html;

	}

	/**
	 * Erstellt einen Details-Bereich in HTML.
	 *
	 * @param   string  $Titel
	 * @param   string  $Inhalt
	 *
	 * @return string
	 *
	 * @Link https://www.w3schools.com/tags/tag_details.asp
	 * @since 0.0.6
	 */
	public static function erstelleDetails(string $Titel, string $Inhalt): string
	{
		return '<details><summary>' . $Titel . '</summary><p>' . $Inhalt . '</p></details>';
	}

	/**
	 * Gibt eine Datalist zurück.
	 *
	 * @param   string  $ID           Die Listen-ID (list="$ID")
	 * @param   array   $Auswahltopf  Worte, welche als Option erscheinen sollen.
	 * @param   bool    $Java         Soll die Datalist mit Javascript betrieben werden.
	 *
	 * @return string
	 *
	 * @link    https://dev.to/siddev/customise-datalist-45p0 für Javascirpt nach diesem Prinzip
	 *
	 * @since   0.0.8
	 * @version 0.1.0
	 */
	public static function erstelleDatalist(string $ID, array $Auswahltopf, bool $Java = false): string
	{
		$html = '<datalist id="' . $ID . '">';
		foreach ($Auswahltopf as $Auswahl)
		{
			$html .= '<option value="' . str_replace('"', "", $Auswahl) . '">';
			if ($Java)
			{
				if (self::istMobil() and strlen($Auswahl) > 40)
				{
					$html .= mb_substr($Auswahl, 0, 40) . ' ...</option>';
				}
				else
				{
					$html .= $Auswahl . '</option>';
				}
			}

		}
		$html .= '</datalist>';

		return $html;
	}

	/**
	 * Erstellt einfache Formularfelder.
	 * In ein Array verpackt müssen folgende Informationen mitgegeben werden:
	 * Type → {hidden, text, number, etc.}
	 * Name → {ist auch die ID des Formulars}
	 * Titel → {die sichtbare Beschriftung}
	 * Value → {den Wert, der übergeben werden soll}
	 *
	 *
	 * @param   array  $Formulare
	 *
	 * @return string
	 * @since    0.0.9
	 * @version  0.3.2
	 */
	public static function erstelleFormularfelder(array $Formulare): string
	{

		// Für Einzelarrays
		if (count($Formulare) == 4)
		{
			$Daten = array(0 => $Formulare);
		}
		else
		{
			$Daten = $Formulare;
		}

		$htmlFormularfelder = '';
		if ($Daten)
		{
			foreach ($Daten as $Zeile)
			{

				// Das Formular zusammenstellen
				if ($Zeile['Type'] !== 'hidden')
				{
					$htmlFormularfelder .= '<label for="' . $Zeile['Name'] . '">' . $Zeile['Titel'] . '</label><br>';
				}
				$htmlFormularfelder .= '<input type="' . $Zeile['Type'] . '" id="' . $Zeile['Name'] . '" name="' . $Zeile['Name'] . '" value="' . $Zeile['Value'] . '"/>';
			}
		}

		return $htmlFormularfelder;
	}

	/**
	 * Erstellt einen einfachen Link. Damit wird sichergestellt, dass die Qualität stimmt.
	 *
	 * @param   string  $Link    Die Zieladresse
	 * @param   string  $Text    Der angezeigte Link
	 * @param   string  $Title   Der Titel, wenn nicht angezeigt entspricht er dem angezeigten Link. Alle doppelte Anführungszeichen werden zu Einfachen umgestellt, damit der Link korrekt ausgegeben wird.
	 * @param   string  $Target  Standardmässig wird der Link im selben Fenster geöffnet, ansonsten muss '_blank' angegeben werden.
	 * @param   string  $Style   Einen benutzerdefinierten Style.
	 * @param   string  $Klasse  Die CSS Klasse des Links.
	 * @param   string  $Rel     Hier kann man z.B. ein 'nofollow' Tag anhängen.
	 *
	 * @return string
	 * @since   0.0.1
	 * @version 0.4.0
	 */
	public static function erstelleLink(string $Link, string $Text, string $Title = '', string $Target = '_self', string $Style = '', string $Klasse = '', string $Rel = ''): string
	{

		// Title übernehmen, wenn nicht gesetzt
		if (empty($Title))
		{
			$Title = str_replace('"', "'", strip_tags($Text));
		}
		else
		{
			$Title = str_replace('"', "'", $Title);
		}
		// Style des Links
		if (!empty($Style))
		{
			$Style = ' style="' . $Style . '" ';
		}
		// Klasse des Links
		if (!empty($Klasse))
		{
			$Klasse = ' class="' . $Klasse . '" ';
		}
		// Rels
		if (!empty($Rel))
		{
			$Rel = ' ' . $Rel;
		}

		return '<a href="' . $Link . '" title="' . $Title . '" target="' . $Target . '" rel="noopener' . $Rel . '"' . $Style . $Klasse . '>' . $Text . '</a>';
	}

	/**
	 * Formatiert ein beliebiges Datum.
	 *
	 * @param   string   $Start   Das (Start)Datum.
	 * @param   string   $Ende    Das Enddatum (optional).
	 * @param   boolean  $Tage    Wenn "true" werden die Daten mit Wochentag-Namen ergänzt (optional).
	 * @param   boolean  $Monate  Wenn "true" werden die Monate mit Namen wiedergegeben (optional).
	 *
	 * @return string Mit dem formatierten Datum
	 * @since  0.0.4
	 */
	public static function formatiereDatum(string $Start, string $Ende = '', bool $Tage = false, bool $Monate = false)
	{

		$MehrereTage = $Start == $Ende ? false : true;

		$Tagesnamen  = array("Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag");
		$Monatsnamen =
			array(
				1  => "Januar",
				2  => "Februar",
				3  => "März",
				4  => "April",
				5  => "Mai",
				6  => "Juni",
				7  => "Juli",
				8  => "August",
				9  => "September",
				10 => "Oktober",
				11 => "November",
				12 => "Dezember"
			);

		$Start = date_create($Start);

		$TagStart   = $Tage ? $Tagesnamen[date_format($Start, "w")] . ', den ' . date_format($Start, "d") . '.' : date_format($Start, "d") . '.';
		$MonatStart = $Monate ? ' ' . $Monatsnamen[date_format($Start, "n")] . ' ' : date_format($Start, "m") . '.';
		$JahrStart  = date_format($Start, "Y");


		if ($Ende and $MehrereTage)
		{
			$Ende = date_create($Ende);

			$TagEnde   = $Tage ? $Tagesnamen[date_format($Ende, "w")] . ', den ' . date_format($Ende, "d") . '.' : date_format($Ende, "d") . '.';
			$MonatEnde = $Monate ? ' ' . $Monatsnamen[date_format($Ende, "n")] . ' ' : date_format($Ende, "m") . '.';
			$JahrEnde  = date_format($Ende, "Y");

			$Datum = $TagStart;
			if ($MonatStart !== $MonatEnde)
			{
				$Datum .= $MonatStart;
			}
			if ($JahrStart !== $JahrEnde)
			{
				$Datum .= $JahrStart;
			}

			$Datum .= ' - ' . $TagEnde . $MonatEnde . $JahrEnde;
		}
		else
		{
			$Datum = $TagStart . $MonatStart . $JahrStart;
		}

		return $Datum;

	}


	/**
	 * Prüft ob der Benutzer mit einem Mobilgerät auf die Seite zugreift
	 *
	 * @return bool
	 *
	 * @since   0.0.1
	 * @version 0.3.1
	 */
	public static function istMobil(): bool
	{
		$user_agent = $_SERVER["HTTP_USER_AGENT"] ?? '';

		if (preg_match("/(android|avantgo|iphone|ipod|ipad|bolt|boost|cricket|docomo|fone|hiptop|opera mini|mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $user_agent))
		{
			//echo "mobile device detected" . $user_agent;
			$_SESSION['USER_MOBIL_AGENT'] = $user_agent;

			return true;
		}
		else
		{
			// echo "mobile device not detected" . $user_agent;
			return false;
		}
	}

	/**
	 * Konvertiert eine RGB(A) Wert zu einem HEX Wert.
	 *
	 * @param   string  $rgba
	 *
	 * @return string
	 * @since 0.0.6
	 * @link  https://stackoverflow.com/questions/76975572/how-convert-rgba-to-hex6-color-code-in-php
	 *
	 */
	public static function konvertiereRGBzuHEX(string $rgba): string
	{

		$rgba = strtolower(trim($rgba));

		// falls rgb() statt rgba() verwendet wurde
		if (str_starts_with($rgba, 'rgb('))
		{
			$rgba = str_replace(array('rgb', ')'), array('rgba', ',1.0)'), $rgba);
		}

		// check
		if (!str_starts_with($rgba, 'rgba('))
		{
			return $rgba;
		}

		// korrigiert false Alphakanaleingaben
		$rgba = preg_replace(array('/,(\.\d\))$/', '/,1\)$/'), array(',0$1', ',1.0)'), $rgba);

		// extract channels
		$channels = explode(',', substr($rgba, 5, strpos($rgba, ')') - 5));
		// compute rgb with white background
		$alpha = $channels[3];
		$r     = GMF_Layout::blendChannels($alpha, $channels[0], 0xFF);
		$g     = GMF_Layout::blendChannels($alpha, $channels[1], 0xFF);
		$b     = GMF_Layout::blendChannels($alpha, $channels[2], 0xFF);

		return sprintf('#%02x%02x%02x', $r, $g, $b);
	}

	public static function blendChannels(float $alpha, int $channel1, int $channel2): int
	{
		// Kann verändert werden
		$gamma = 2.2;

		// blend 2 channels
		return intval(pow((pow($channel1, $gamma) * $alpha) +
			(pow($channel2, $gamma) * (1.0 - $alpha)), 1 / $gamma));
	}

}
