<?php

// Kein direkter Aufruf der PHP-Datei
defined('_JEXEC') or die('Restricted Access');

use Joomla\CMS\Plugin\PluginHelper;
use Joomla\Registry\Registry;
use Joomla\CMS\Factory;
use Joomla\CMS\Filter\OutputFilter;

class GMF_Sicherheit {

	/**
	 * Ersetzt den aktuellen History Eintrag.
	 * So kann verhindert werden, dass Variablen doppelt übermittelt werden.
	 *
	 * @return string
	 * @link https://developer.mozilla.org/en-US/docs/Web/API/History/replaceState
	 * @since 0.0.9
	 */
	public static function ersetzeParameterHistory() : string {
		return '<script>
                    if ( window.history.replaceState ) {
                        window.history.replaceState( null, null, window.location.href );
                    } 
                </script>';
	}

	/**
	 * Generiert ein sicheres Passwort mit 10 - 20 Zeichen.
	 *
	 * @param   string  $Text Text, aus dem das Passwort generiert wird.
	 * @return string
	 * @since 0.2.0
	 * @version 0.3.3
	 */
	public static function generierePasswort(string $Text): string
	{
		if($Text) {
			$Fehler = false;

			// 1 Zeichen
			$StartZeichenArray = ['?','-','!',':'];
			$Passwort[] = $StartZeichenArray[mt_rand(0,3)];

			$Array = str_word_count($Text, 1,'ÄÖÜäöüß');
			$AnzahlWorte = count($Array)-1;

			$WortEins = 'kein';
			$ZufallKey = 0;
			$a = 0;
			// 4-8 Zeichen
			while (!preg_match('/^[A-ZÄÖÜ][a-zäöüß]{3,7}$/',$WortEins) and $a < 50) {
				$ZufallKey = mt_rand(0,$AnzahlWorte);
				$WortEins = $Array[$ZufallKey];
				$a++;
			}

			// Rechtschreibfehler machen, damit es nicht ein Wort aus dem Wörterbuch ist
			$SelbstlauteArray = array('a','e','i','o','u');
			shuffle($SelbstlauteArray);

			// Anfang oder Wortende?
			$ZahlLazy = mt_rand(0,1);
			$Lazy = $ZahlLazy ? 'U' : '';

			$Selbstlaute = implode("",$SelbstlauteArray);
			$Suche = preg_replace('/^.*([' . $Selbstlaute . ']).*$/' . $Lazy,'$1',$WortEins);
			$Ersetzen = $Suche . $Suche;

			// +1 Zeichen
			$pos = strpos($WortEins,$Suche);
			if ($pos !== false and strlen($Suche) == 1) {
				$WortEins = substr_replace($WortEins, $Ersetzen, $pos,1);
			} else {
				$Fehler = true;
			}
			$Passwort[] = $WortEins;

			// 1-2 Zahlen
			$Passwort[] = $a;

			if(isset($_GET['Stichwort']) and preg_match('/^[a-zäöüéè]+$/iu', trim($_GET['Stichwort']))) {
				$Passwort[] = trim($_GET['Stichwort']);
			}
			else
			{
				$WortZwei = 'Nein';
				$b        = 0;
				// 4-8 Zeichen
				while (!preg_match('/^[a-zäöüß]{4,8}$/', $WortZwei) and $b < 50)
				{
					$ZufallKey = mt_rand(0, $AnzahlWorte);
					$WortZwei  = $Array[$ZufallKey];
					$b++;
				}
			}

			$Passwort[] = $WortZwei;
			shuffle($Passwort);
			// Passwort zusammensetzen
			$PasswortString = implode("",$Passwort);


			if($a == 50 or $b == 50 or $Fehler) {
				$PasswortString .= ' <span style="color: red;" title="Passwort entspricht möglicherweise nicht den Vorgaben!">!</span>';
			}

			return $PasswortString;
		} else {
			return 'Kein Eingabe-Text vorhanden!';
		}

	}

	/**
	 * Erstellt ein Honeypot-Feld
	 * 
	 * @link https://help.salesforce.com/s/articleView?id=sf.pardot_forms_add_honeypot.htm&type=5
	 * @link https://dev.to/felipperegazio/how-to-create-a-simple-honeypot-to-protect-your-web-forms-from-spammers--25n8
	 * @since 0.0.3
	 * @version 0.0.5
	 * @return string
	 */
	public static function initialisieFormularsicherheit() {
		return '<div style="position:absolute; left: 0; bottom: 0; opacity: 0; z-index: -1">
					<label for="gratia-honigpot">Kontrollfeld*</label>
					<input type="text" id="gratia-honigpot" name="gm-honigpot" value="" title="Ein Honigpot-Kontrollfeld gegen Spam.">
				</div>';
	}
		
	/**
	 * Initialisiert ReCaptcha invisible. Die Key werden vom Joomla-Plugin übernommen. Unterstützt wird auch das Linksbündige Layout. Beides muss über das Systemplugin von Jooomla konfiguriert werden. Die Funktion muss im <form>-Tag aufgerufen werden.
	 *
	 * @return string Der Rückgabewert enthält zwei wichtige versteckte «Input»-Felder, die nötig für ReCaptcha sind.
	 * @since 0.0.1
	 */
	public static function initialisieReCaptcha() {
	
		
		// Falls ReCaptcha invisible aktiviert ist
		if(PluginHelper::isEnabled('captcha','recaptcha_invisible')) {
			// Einstellungen auslesen
			$plugin = PluginHelper::getPlugin('captcha', 'recaptcha_invisible');
			$CaptchaParams = new Registry($plugin->params);

			// JavaScript ReCaptcha hinzufügen 
			$document = Factory::getDocument();
			$document->addScript('https://www.google.com/recaptcha/api.js?render=' . $CaptchaParams->get('public_key'));
			$Skript =	"grecaptcha.ready(function () {
							grecaptcha.execute('" . $CaptchaParams->get('public_key') . "', { action: 'validate_captcha' }).then(function (token) {
								var recaptchaResponse = document.getElementById('recaptchaResponse');
								recaptchaResponse.value = token;
							});
						})";
			$document->addScriptDeclaration($Skript);
			// CSS hinzufügen
			if($CaptchaParams->get('badge') == 'bottomleft') {
				$document->addStyleDeclaration('
					.grecaptcha-badge {
						width: 70px !important;
						overflow: hidden !important;
						transition: all 0.3s ease !important;
						left: 4px !important;
					}
					.grecaptcha-badge:hover {
						width: 256px !important;
					}');
			}
	
			// Rückgabe für den Formularwert
			return '<input type="hidden" name="recaptcha_response" id="recaptchaResponse"><input type="hidden" name="action" value="validate_captcha">';
		}		
	}


	/**
	 * Wertet die Formular-Eingabe der Funktion "initialisieFormularsicherheit" aus.
	 *
	 * @param boolean $Duplikat Soll die Eingabe auf ein Duplikat geprüft werden?
	 * @param array $Eingabe Array mit allen Parameter-Inhalten, welche geprüft werden sollen.
	 * @param boolean $Honeypot
	 * @since 0.0.3
	 * @return boolean
	 */
	public static function pruefeFormularsicherheit(bool $Duplikat = true , array $Eingabe = array(),bool $Honeypot = true) {

		if($Honeypot) {
			if(!empty($_REQUEST['gm-honigpot']))
				return false;
		}

		if($Duplikat) {
			$Duplikate = 0;
			foreach($Eingabe as $Parameter) {
				
				$Parameter = OutputFilter::stringUrlSafe($Parameter);

				if(isset($_SESSION['duplikatskontrolle' . $Parameter]) and $_SESSION['duplikatskontrolle' . $Parameter] == 'duplikatskontrolle' . $Parameter) {
					$Duplikate++;
				} else {
					$_SESSION['duplikatskontrolle' .  $Parameter] = 'duplikatskontrolle' . $Parameter;
				}
			}
			if(count($Eingabe) == $Duplikate) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Prüft die ReCaptcha-Antwort und gibt true oder false zurück.
	 * @var int $Score Über diesen Parameter kann gesteuert werden, wie hoch der Score sein soll. 
	 * @return boolean Der Score muss 0.3 oder höher sein, damit true erreicht wird.
	 * @since 0.0.1
	 * @version 0.0.3
	 */
	public static function pruefeReCaptcha($Score = 0.3) {

		// Falls ReCaptcha invisible aktiviert ist
		if(PluginHelper::isEnabled('captcha','recaptcha_invisible')) {
			// Einstellungen auslesen
			$plugin = PluginHelper::getPlugin('captcha', 'recaptcha_invisible');
			$CaptchaParams = new Registry($plugin->params);
			
			if (isset($_REQUEST['recaptcha_response'])) {
				$captcha = $_REQUEST['recaptcha_response'];
			}
			
			if (!$captcha) {
				return false;
			} else {
				$secret   = $CaptchaParams->get('private_key');
				$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
				// use json_decode to extract json response
				$response = json_decode($response);
				// print_r($response);
			
				if ($response->success === false) {
					return false;
				}
			}
			
			//... The Captcha is valid you can continue with the rest of your code
			//... Add code to filter access using $response . score
			if ($response->success==true && $response->score <= $Score) {
				return false;
			} else {
				return true;
			}
		} else {
			// Falls ReCaptcha nicht eingeschaltet, eine positive Antwort zurückgeben
			return true;
		}
	}
}
?>
