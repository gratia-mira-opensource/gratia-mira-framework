<?php

/**
 * @author          Christoph J. Berger <w@gratia-mira.ch>
 * @link            https://gratia-mira.ch
 * @copyright       Copyright © 2023 gratia-mira.ch
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
 * @since 0.0.1
 * @version 0.0.4
*/

defined( '_JEXEC' ) or die( 'Restricted access' );

// Klasse laden
require_once __DIR__ . '/framework/gmf_allgemein.php';
require_once __DIR__ . '/framework/gmf_layout.php';
require_once __DIR__ . '/framework/gmf_sicherheit.php';
require_once __DIR__ . '/framework/gmf_gratiabruecke.php';
require_once __DIR__ . '/libraries/forceutf8.php';

if(file_exists(dirname(__FILE__,4) . '/modules/mod_bibelversanzeige/php/tools.php')) {
    require_once dirname(__FILE__,4) . '/modules/mod_bibelversanzeige/php/benutzereingabeanalysator.php';
    require_once dirname(__FILE__,4) . '/modules/mod_bibelversanzeige/php/tools.php';
    require_once dirname(__FILE__,4) . '/modules/mod_bibelversanzeige/php/konstanten.php';
	new Konstanten();
    define('GM_MOD_BIBELVERSANZEIGE',true);
}

class plgSystemGMFramework extends JPlugin
{

 	/**
     *  Plugin constructor
     *
     *  @param  mixed   &$subject
     *  @param  array   $config
	 *  @since  0.0.1
	 *  @version 0.0.8
     */
    public function __construct(&$subject, $config = array())
    {   
        // Veraltet im Mai 2023 (0.0.3)
        define('GMFramework','0.3.1');
        // Damit man prüfen kann, ob das Framework geladen wurde
        define('GM_FRAMEWORK','0.3.1');
    }
}