<?php

/**
 * $converter = new Zahl2Text();
 * echo $converter->convert(1234567); // gibt "eine Million zweihundertvierunddreißigtausend fünfhundertsiebenundsechzig" zurück
 *
 * @author    Thorsten Rotering <support@rotering-net.de>
 * @copyright Gratia-Mira (2024)
 * @version   1.1 (2017-08-06)
 *
 * @license   Hiermit wird unentgeltlich, jeder Person, die eine Kopie dieses Skripts erhält, die Erlaubnis erteilt,
 * diese uneingeschränkt zu benutzen, inklusive und ohne Ausnahme, dem Recht, sie zu verwenden, zu kopieren,
 * zu ändern, zu fusionieren, zu verlegen, zu verbreiten, zu unterlizenzieren und/oder zu verkaufen, und
 * Personen, die dieses Skript erhalten, diese Rechte zu geben, unter den folgenden Bedingungen:
 *
 * Der obige Urheberrechtsvermerk und dieser Erlaubnis-Vermerk sind in allen Kopien oder Teilkopien des
 * Skripts beizulegen.
 *
 * DAS SKRIPT WIRD OHNE JEDE AUSDRÜCKLICHE ODER IMPLIZIERTE GARANTIE BEREITGESTELLT, EINSCHLIESSLICH DER
 * GARANTIE ZUR BENUTZUNG FÜR DEN VORGESEHENEN ODER EINEM BESTIMMTEN ZWECK SOWIE JEGLICHER RECHTSVERLETZUNG,
 * JEDOCH NICHT DARAUF BESCHRÄNKT. IN KEINEM FALL SIND DIE AUTOREN ODER COPYRIGHT INHABER FÜR JEGLICHEN SCHADEN
 * ODER SONSTIGE ANSPRÜCHE HAFTBAR ZU MACHEN, OB INFOLGE DER ERFÜLLUNG EINES VERTRAGES, EINES DELIKTES ODER
 * ANDERS IM ZUSAMMENHANG MIT DEM SKRIPT ODER SONSTIGER VERWENDUNG DES SKRIPTS ENTSTANDEN.
 * @since     0.3.0
 */

class Zahl2Text
{
	private const NUMERAL_SIGN = 'minus';
	private const NUMERAL_HUNDREDS_SUFFIX = 'hundert';
	private const NUMERAL_INFIX = 'und';

	private array $numerals = [
		'null', 'ein', 'zwei', 'drei', 'vier',
		'fünf', 'sechs', 'sieben', 'acht', 'neun',
		'zehn', 'elf', 'zwölf', 'dreizehn', 'vierzehn',
		'fünfzehn', 'sechzehn', 'siebzehn', 'achtzehn', 'neunzehn'
	];

	private array $tenners = [
		'', '', 'zwanzig', 'dreißig', 'vierzig',
		'fünfzig', 'sechzig', 'siebzig', 'achtzig', 'neunzig'
	];

	private array $groupSuffixes = [
		['s', ''],
		['tausend ', 'tausend '],
		['e Million ', ' Millionen '],
		['e Milliarde ', ' Milliarden '],
		['e Billion ', ' Billionen '],
		['e Billiarde ', ' Billiarden '],
		['e Trillion ', ' Trillionen ']
	];

	/**
	 * Liefert das Zahlwort zu einer Ganzzahl zurück.
	 *
	 * @param   int  $number  Die Ganzzahl, die in ein Zahlwort umgewandelt werden soll.
	 *
	 * @return string Das Zahlwort.
	 * @since 0.3.0
	 */
	public function convert(int $number): string
	{
		if ($number == 0)
		{
			return trim($this->numerals[0]);
		}
		elseif ($number < 0)
		{
			return trim(self::NUMERAL_SIGN . ' ' . $this->convertGroup(abs($number)));
		}
		else
		{
			return trim($this->convertGroup($number));
		}
	}

	/**
	 * Rekursive Methode, die das Zahlwort zu einer Ganzzahl zurückgibt.
	 *
	 * @param   int  $number      Die Ganzzahl, die in ein Zahlwort umgewandelt werden soll.
	 * @param   int  $groupLevel  (optional) Das Gruppen-Level der aktuellen Zahl.
	 *
	 * @return string Das Zahlwort.
	 * @since 0.3.0
	 */
	private function convertGroup(int $number, int $groupLevel = 0): string
	{
		if ($number == 0)
		{
			return '';
		}

		$groupNumber = $number % 1000;
		$result      = '';

		if ($groupNumber == 1)
		{
			$result = $this->numerals[1] . $this->groupSuffixes[$groupLevel][0]; // eine Milliarde
		}
		elseif ($groupNumber > 1)
		{
			$firstDigit = floor($groupNumber / 100);

			if ($firstDigit > 0)
			{
				$result .= $this->numerals[$firstDigit] . self::NUMERAL_HUNDREDS_SUFFIX; // fünfhundert
			}

			$lastDigits  = $groupNumber % 100;
			$secondDigit = floor($lastDigits / 10);
			$thirdDigit  = $lastDigits % 10;

			if ($lastDigits == 1)
			{
				$result .= $this->numerals[1] . 's'; // "eins"
			}
			elseif ($lastDigits > 1 && $lastDigits < 20)
			{
				$result .= $this->numerals[$lastDigits]; // "dreizehn"
			}
			elseif ($lastDigits >= 20)
			{
				if ($thirdDigit > 0)
				{
					$result .= $this->numerals[$thirdDigit] . self::NUMERAL_INFIX; // "sechsund-"
				}
				$result .= $this->tenners[$secondDigit]; // "-achtzig"
			}

			$result .= $this->groupSuffixes[$groupLevel][1]; // "Millionen"
		}

		$Zahlenwort = $this->convertGroup(floor($number / 1000), $groupLevel + 1) . $result;

		// Ausnahmen
		$Suchen = ['eintausend','einhundert'];
		$Ersetzen = ['tausend','hundert'];


		return str_replace($Suchen, $Ersetzen, $Zahlenwort);
	}
}
